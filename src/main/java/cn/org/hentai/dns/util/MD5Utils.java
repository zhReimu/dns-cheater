package cn.org.hentai.dns.util;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;

public class MD5Utils {
    public static void main(String[] args) {
        System.out.println(MD5Utils.encode("web:::127.0.0.1:::29:::;S,I(\\\"**%k#/:!7D").length());
    }

    public static String encode(String s) {
        try {
            return MD5(s.getBytes(StandardCharsets.UTF_8));
        } catch (Exception ex) {
            return null;
        }
    }

    public static String encode(byte[] buf) {
        return MD5(buf);
    }

    private static String MD5(byte[] btInput) {
        char[] hexDigits = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};
        try {
            MessageDigest mdInst = MessageDigest.getInstance("MD5");
            mdInst.update(btInput);
            byte[] md = mdInst.digest();
            int j = md.length;
            char[] str = new char[j * 2];
            int k = 0;
            for (byte byte0 : md) {
                str[k++] = hexDigits[byte0 >>> 4 & 0xf];
                str[k++] = hexDigits[byte0 & 0xf];
            }
            return new String(str);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

}
