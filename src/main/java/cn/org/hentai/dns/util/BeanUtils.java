package cn.org.hentai.dns.util;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.context.ApplicationContext;

/**
 * Created by Expect on 2018/1/25.
 */
public final class BeanUtils {
    private static BeanFactory beanFactory;

    public static void init(ApplicationContext context) {
        BeanUtils.beanFactory = context;
    }

    public static <T> T getBean(Class<T> targetClass) {
        return beanFactory.getBean(targetClass);
    }

}
