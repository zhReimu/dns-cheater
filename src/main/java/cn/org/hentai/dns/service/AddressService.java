package cn.org.hentai.dns.service;

import cn.org.hentai.dns.dao.AddressMapper;
import cn.org.hentai.dns.entity.Address;
import cn.org.hentai.dns.entity.AddressExample;
import cn.org.hentai.dns.entity.Rule;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by matrixy on 2019/4/28.
 */
@Service
@RequiredArgsConstructor
public class AddressService {

    private final AddressMapper addrMapper;

    public void create(Address addr) {
        addrMapper.insert(addr);
    }

    public int update(Address addr) {
        return addrMapper.updateByPrimaryKey(addr);
    }

    public int remove(Address addr) {
        return addrMapper.deleteByPrimaryKey(addr.getId());
    }

    public List<Address> find(Long ruleId) {
        return addrMapper.selectByExample(new AddressExample().createCriteria().andRuleIdEqualTo(ruleId).example());
    }

    public void removeByRule(Rule rule) {
        addrMapper.deleteByExample(new AddressExample().createCriteria().andRuleIdEqualTo(rule.getId()).example());
    }
}
