package cn.org.hentai.dns.service;

import cn.org.hentai.dns.dao.RuleMapper;
import cn.org.hentai.dns.entity.Page;
import cn.org.hentai.dns.entity.Rule;
import cn.org.hentai.dns.entity.RuleExample;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

/**
 * Created by matrixy on 2019/4/28.
 */
@Service
@RequiredArgsConstructor
public class RuleService {

    private final RuleMapper ruleMapper;

    public void create(Rule rule) {
        ruleMapper.insert(rule);
    }

    public void update(Rule rule) {
        ruleMapper.updateByPrimaryKey(rule);
    }

    public void remove(Rule rule) {
        remove(rule.getId());
    }

    public int remove(Long id) {
        return ruleMapper.deleteByPrimaryKey(id);
    }

    public Rule getById(Long id) {
        return ruleMapper.selectByPrimaryKey(id);
    }

    public Page<Rule> find(int pageIndex, int pageSize) {
        Page<Rule> page = new Page<>(pageIndex, pageSize);
        RuleExample.Criteria criteria = new RuleExample().createCriteria();
        criteria.example().setOrderByClause(Rule.Column.id.desc());
        criteria.example().setPageInfo(pageIndex, pageSize);
        page.setList(ruleMapper.selectByExample(criteria.example()));
        page.setRecordCount(ruleMapper.countByExample(criteria.example()));
        return page;
    }
}
