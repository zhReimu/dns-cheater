package cn.org.hentai.dns;

import cn.org.hentai.dns.protocol.NameServer;
import cn.org.hentai.dns.protocol.RecursiveResolver;
import cn.org.hentai.dns.protocol.RuleManager;
import cn.org.hentai.dns.util.BeanUtils;
import cn.org.hentai.dns.util.Configs;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;
import org.springframework.context.ApplicationContext;

/**
 * Created by matrixy on 2019/4/19.
 */
@MapperScan("cn.org.hentai.dns.dao")
@SpringBootApplication
@ConfigurationPropertiesScan(basePackages = {"cn.org.hentai.dns.config"})
public class DNSCheaterApp {

    public static void main(String[] args) {
        ApplicationContext context = SpringApplication.run(DNSCheaterApp.class, args);
        BeanUtils.init(context);
        Configs.init(context);

        RuleManager.getInstance().init();
        NameServer.getInstance().init();
        RecursiveResolver.getInstance().init();
    }
}
