package cn.org.hentai.dns.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author Mr.X
 * @since 2022-05-04-0004
 **/
@Data
@Component
@ConfigurationProperties(prefix = "dns.server")
public class DNSServerProperties {

    private Integer port = 53;

    private String address;

    private String name;

}
